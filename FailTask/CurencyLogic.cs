﻿using System;
using System.IO;

namespace FailTask
{
    class CurencyLogic
    {
        static void Main()
        {
            Curency mainCurency = CreateMainCurency();

            foreach(string[] info in mainCurency.Prices)
            {
                CreateNewCurencyDocument(info, mainCurency);
            }
        }

        static Curency CreateMainCurency()
        {
            string path = Path.Combine(Environment.CurrentDirectory, "data.txt");
            string[] data = File.ReadAllLines(path);

            string[][] prices = new string[data.Length-1][];
            for (int i = 1; i < data.Length; i++)
            {
                string[] parts = data[i].Split(' ');
                prices[i - 1] = parts;
            }

            Curency mainCurency = new Curency
            {
                Name = data[0],
                Prices = prices,
            };

            return mainCurency;
        }

        static void CreateNewCurencyDocument(string[] info, Curency mainCurency)
        {
            string[][] prices = new string[mainCurency.Prices.Length][];
            for (int i = 0; i < prices.Length; i++)
            {
                string name;
                string soldingPrice;
                string buyingPrice;

                if (info[0] == mainCurency.Prices[i][0])
                {
                    name = mainCurency.Name;
                    soldingPrice = Calculator.CalculatePrice(info[1], "1,00");
                    buyingPrice = Calculator.CalculatePrice(info[2], "1,00");
                }
                else
                {
                    name = mainCurency.Prices[i][0];
                    soldingPrice = Calculator.CalculatePrice(info[1], mainCurency.Prices[i][1]);
                    buyingPrice = Calculator.CalculatePrice(info[2], mainCurency.Prices[i][2]);
                }

                string[] parts = new string[]
                {
                    name,
                    soldingPrice,
                    buyingPrice,
                };

                prices[i] = parts; 
            }

            Curency curency = new Curency
            {
                Name = info[0],
                Prices = prices,
            };

            DataWriter.WriteData(curency);
        }
    }
}
