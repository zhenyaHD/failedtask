﻿using System;
using System.Globalization;

namespace FailTask
{
    public static class Calculator
    {
        public static string CalculatePrice(string firstPrice, string secondPrice)
        {
            double firstNum = double.Parse(firstPrice, CultureInfo.InvariantCulture);
            double secondNum = double.Parse(secondPrice, CultureInfo.InvariantCulture);

            double price = firstNum / secondNum;
            price = Math.Round(price, 2);

            string strPrice = price.ToString();

            if (!strPrice.Contains(',')) strPrice += ",00";
            if (strPrice[strPrice.Length - 2] == ',') strPrice += "0";

            return strPrice;
        }
    }
}
