﻿using System;
using System.IO;

namespace FailTask
{
    public static class DataWriter
    {
        public static void WriteData(Curency curency)
        {
            string[] lines = new string[curency.Prices.Length + 1];
            lines[0] = curency.Name;

            for (int i = 1; i < lines.Length; i++)
            {
                lines[i] = $"{curency.Prices[i - 1][0]} {curency.Prices[i - 1][1]} {curency.Prices[i - 1][2]}";
            }

            string path = Path.Combine(Environment.CurrentDirectory, $"{curency.Name}.txt");
            File.WriteAllLines(path, lines);
        }
    }
}
